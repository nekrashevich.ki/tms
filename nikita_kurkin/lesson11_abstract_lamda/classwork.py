from abc import ABC, abstractmethod


class Pet(ABC):
    @abstractmethod
    def move(self):
        print('halo')


class Parrot(Pet):
    def move(self):
        print('I can fly!')


p1 = Parrot()
p1.move()

"""
Методы класса Animal:
    Идти
    Спать
    Есть
    пить
Pet:
    любить
    есть_тапки

WildAnimal:
    охотится
    рыть_яму
    рычать
"""


class Animal(ABC):
    @abstractmethod
    def go(self):
        print('go')

    @abstractmethod
    def sleep(self):
        print('sleep')

    @abstractmethod
    def eat(self):
        print('eat')

    @abstractmethod
    def drink(self):
        print('drink')


class Pet(Animal):
    @abstractmethod
    def loving(self):
        print('love')

    @abstractmethod
    def eat_shoes(self):
        print('eat shoes')


class WildAnimal(Animal):
    @abstractmethod
    def hunt1(self):
        print('hunt')

    @abstractmethod
    def roar(self):
        print('roar')

    @abstractmethod
    def dig_a_hole(self):
        print('dig a hole')


class Cat(Pet):
    def eat(self):
        print('cat eat')

    def drink(self):
        print('cat drink')

    def go(self):
        print('cat go')

    def sleep(self):
        print('cat sleep')

    def loving(self):
        print('I am a cat! I am loving everyone')

    def eat_shoes(self):
        print('I have to pee in your shoes!')


cat1 = Cat()
cat1.loving()


class Dog(Pet):
    def eat(self):
        print('dog eat')

    def drink(self):
        print('dog drink')

    def go(self):
        print('dog go')

    def sleep(self):
        print('dog sleep')

    def loving(self):
        print('I am a dog! I am loving everyone')

    def eat_shoes(self):
        print('I have to pee in your shoes!')


dog1 = Dog()
dog1.eat_shoes()


class Lion(WildAnimal):
    def hunt1(self):
        print('passive hunt')

    def roar(self):
        print('roar')

    def dig_a_hole(self):
        print('dig a hole')

    def go(self):
        print('go')

    def sleep(self):
        print('sleep')

    def eat(self):
        print('eat')

    def drink(self):
        print('drink')


lion1 = Lion()
lion1.eat()


class Wolf(WildAnimal):
    def hunt1(self):
        print('woof hunt')

    def roar(self):
        print('woof')

    def dig_a_hole(self):
        print('dig a hole')

    def go(self):
        print('go')

    def sleep(self):
        print('sleep')

    def eat(self):
        print('eat')

    def drink(self):
        print('drink')


wolf1 = Wolf()
wolf1.roar()

"""
# количество страниц > 4000
# год < 1980
# автор не должен содержать других символов кроме букв
# цена не должна быть меньше 100 и больше 10000
"""


class PageNumberError(Exception):
    message = 'Wrong count of pages'

    def __init__(self):
        super().__init__(self.message)


class YearError(Exception):
    message = 'Wrong year'

    def __init__(self):
        super().__init__(self.message)


class AuthorError(Exception):
    message = 'Author should be only letters'

    def __init__(self):
        super().__init__(self.message)


class PriceError(Exception):
    message = 'Wrong price'

    def __init__(self):
        super().__init__(self.message)


class Book:
    def __init__(self, count_of_pages, year, author, price):
        self.count_of_pages = self._valid_count_pages(count_of_pages)
        self.year = self._valid_year(year)
        self.author = self._valid_author(author)
        self.price = self._valid_price(price)

    @staticmethod
    def _valid_count_pages(count_of_pages):
        if count_of_pages < 4000:
            raise PageNumberError
        return count_of_pages

    @staticmethod
    def _valid_year(year):
        if year > 1980:
            raise YearError
        return year

    @staticmethod
    def _valid_author(author):
        if not author.isalpha():
            raise AuthorError
        return author

    @staticmethod
    def _valid_price(price):
        if not 100 < price < 10000:
            raise PriceError
        return price


Harry_Potter = Book(5000, 1970, 'Rowling', 1000)

"""Создать lambda функцию, которая принимает
на вход имя и выводит его в формате “Hello, {name}”
!!!Ниже закомменченные решения - актуальные!!!"""

# name1 = lambda arg: 'hello, ' + arg
# print(name1('Alex'))

"""Создать lambda функцию, которая
принимает на вход список имен и выводит их в формате
“Hello, {name}” в другой список"""

# name2 = lambda names: [f'hello, {i}' for i in names]
# print(name2(['Alex', 'Sarah']))

print(list(map(lambda names: f'hello, {names}', ['Alex', 'Sarah'])))

"""Дан список чисел. Вернуть список,
где каждый число переведено в строку [5, 3] -> [‘5’, ‘3’]"""

print(list(map(lambda x: str(x), [1, 2, 3, 4, 5])))

"""Дан список имен, отфильтровать все имена, где есть буква k"""
print(list(filter(lambda x: 'k' in x, ['Alexk', 'Sarah', 'Kate'])))


"""s = {"a": 10, "b": 20, "c": 0, "d": -1}
отсортировать словарь с помощью функции sorted()"""
# s = {"a": 10, "b": 20, "c": 0, "d": -1}
# s = dict(sorted(s.items(), key=lambda v: v[1]))
# print(s)
