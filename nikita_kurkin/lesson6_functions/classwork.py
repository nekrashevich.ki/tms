"""
Функции:
1) Напишите функцию, которая возвращает строку: “Hello world!”
"""


def return_hello_world():
    return 'Hello world!'


print(return_hello_world())

"""
2) Напишите функцию, которая вычисляет сумму трех чисел и возвращает
результат в основную ветку программы.
"""


def sum_of_three_digits(a, b, c) -> int:
    return a + b + c


print(sum_of_three_digits(1, 2, 3))

"""
3) Придумайте программу, в которой из одной функции вызывается вторая.
При этом ни одна из них ничего не возвращает в основную ветку программы,
обе должны выводить результаты своей работы с помощью функции print().
"""


def find_the_sqaure(a, b) -> int:
    sqaure_ = a * b
    return sqaure_


def find_the_degree() -> int:
    degree = find_the_sqaure(5, 5) ** 2
    return degree


print(find_the_degree())

"""
4) Напишите функцию, которая не принимает отрицательные числа.
и возвращает число наоборот.
21445 => 54421
123456789 => 987654321
"""


def reverse_numbers(some_digit: int):
    if some_digit > 0:
        reversed_number = int(str(some_digit)[::-1])
        return reversed_number
    else:
        return 'Enter the positive number!'


print(reverse_numbers(23456))
print(reverse_numbers(-23456))

"""
5) Напишите функцию fib(n), которая по данному целому неотрицательному n
возвращает n-e число Фибоначчи.
"""


def fib(n):
    fibonachchi = []
    a, b = 0, 1
    while a < n:
        fibonachchi.append(a)
        a, b = b, a + b
    return fibonachchi[-1]


print(fib(1000))

"""
6) Напишите функцию, которая проверяет на то, является ли строка
палиндромом или нет. Палиндром — это слово или фраза, которые
одинаково читаются слева направо и справа налево.
"""


def palindrome(some_string: str) -> bool:
    s1_keyword = some_string
    s2_palindrome = s1_keyword[::-1]
    return s1_keyword == s2_palindrome


print(palindrome('творог'))
print(palindrome('оно'))

"""
7) У вас интернет магазин, надо написать функцию которая проверяет что введен
правильный купон и он еще действителен

def check_coupon(entered_code, correct_code, current_date, expiration_date):
    #Code here!

check_сoupon("123", "123", "July 9, 2015", "July 9, 2015")  == True
check_сoupon("123", "123", "July 9, 2015", "July 2, 2015")  == False
"""


def check_coupon(entered_code, correct_code, current_date, expiration_date):
    if entered_code == correct_code:
        return current_date == expiration_date


print(check_coupon("123", "123", "July 9, 2015", "July 9, 2015"))
print(check_coupon("123", "123", "July 9, 2015", "July 2, 2015"))

"""
8) Фильтр. Функция принимает на вход список, проверяет есть ли эти элементы в
списке exclude, если есть удаляет их и возвращает список
с оставшимися элементами exclude = ["African", "Roman Tufted",
"Toulouse", "Pilgrim", "Steinbacher"]

filter_list(["Mallard", "Hook Bill", "African", "Crested",
"Pilgrim", "Toulouse",
 "Blue Swedish"]) => ["Mallard", "Hook Bill", "Crested", "Blue Swedish"]
["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"] =>
["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"]
["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"] => []
"""


def filtering(some_list: list) -> list:
    exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
    for i in some_list:
        for j in exclude:
            if i == j:
                some_list.remove(i)
    return some_list


print(filtering(["African", "Roman Tufted", "Toulouse",
                 "Pilgrim", "Steinbacher"]))
