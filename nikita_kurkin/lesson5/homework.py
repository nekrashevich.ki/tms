import random

"""
Быки и коровы
В классическом варианте игра рассчитана на двух игроков.
Каждый из игроков задумывает и записывает тайное 4-значное число
с неповторяющимися цифрами. Игрок, который начинает игру по жребию,
делает первую попытку отгадать число. Попытка — это 4-значное
число с неповторяющимися цифрами, сообщаемое противнику.
Противник сообщает в ответ, сколько цифр угадано без совпадения с
их позициями в тайном числе (то есть количество коров) и сколько
угадано вплоть до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой,
пока не отгадает всю последовательность.
Ваша задача реализовать программу, против которой можно
сыграть в "Быки и коровы"
Пример
Загадано число 3219
 2310
Две коровы, один бык
 3219
Вы выиграли!
"""

some_digit = [i for i in range(1, 10)]
random.shuffle(some_digit)
some_digit = some_digit[:4]
print(some_digit)

while True:
    try_to_guess_digit = input('Введи 4-значное число с неповторяющимися'
                               ' цифрами, которое загадала программа: ')
    try_to_guess_digit = [int(x) for x in str(try_to_guess_digit)]

    bull_counter = 0
    cow_counter = 0
    for i in range(len(some_digit)):
        if some_digit[i] == try_to_guess_digit[i]:
            bull_counter += 1
        elif some_digit[i] in try_to_guess_digit:
            cow_counter += 1

    if bull_counter == 4 and cow_counter == 0:
        print('Поздравляю! Вы выиграли!')
        break
    else:
        print(f'{cow_counter} коровы, {bull_counter} быка!')


'''
Like
Создайте программу, которая, принимая массив имён,
возвращает строку описывающая количество лайков (как в Facebook).

Примеры:
Введите имена через запятую: "Ann"
-> "Ann likes this"
Введите имена через запятую: "Ann, Alex"
-> "Ann and Alex like this"
Введите имена через запятую: "Ann, Alex, Mark"
-> "Ann, Alex and Mark like this"
Введите имена через запятую: "Ann, Alex, Mark, Max"
-> "Ann, Alex and 2 others like this"
Если ничего не вводить должен быть вывод:
-> "No one likes this"
'''

name_list = input('Enter name or names using comma: ')
name_list = name_list.split()
if len(name_list) == 1:
    print(name_list[0], 'like this')
elif len(name_list) == 2:
    print(name_list[0], 'and', name_list[1], 'like this')
elif len(name_list) == 3:
    print(name_list[0], name_list[1], 'and', name_list[2], 'like this')
elif len(name_list) > 3:
    print(name_list[0], name_list[1], 'and', len(name_list) - 2,
          'others like this')
else:
    print('Name field is empty')


'''
BuzzFuzz
Напишите программу, которая перебирает последовательность от 1 до 100.
Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа,
а для чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5
надо печатать "FuzzBuzz". Иначе печатать число.

'''

some_list = [i for i in range(100)]
for i in some_list:
    if i % 3 == 0 and i % 5 == 0:
        print('BuzzFuzz')
    elif i % 3 == 0:
        print('Fuzz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)

'''
Напишите код, который возьмет список строк и пронумерует их.
Нумерация начинается с 1, имеет “:” и пробел
[] => []
["a", "b", "c"] => ["1: a", "2: b", "3: c"]
'''

some_letter_list = ["a", "b", "c", "d"]
some_dict = zip((i + 1 for i in range(len(some_letter_list))),
                some_letter_list)
print(dict(some_dict))

'''
Проверить, все ли элементы одинаковые
[1, 1, 1] == True
[1, 2, 1] == False
['a', 'a', 'a'] == True
[] == True
'''
some_list_with_someone = [1, 2, 1]
for i in some_list_with_someone:
    if some_list_with_someone.count(i) == len(some_list_with_someone):
        print(True)
        break
    elif not some_list_with_someone:
        print(True)
    else:
        print(False)
        break


'''
Проверка строки. В данной подстроке проверить все ли буквы
в строчном регистре или нет и вернуть список не подходящих.

dogcat => [True, []]
doGCat => [False, ['G', 'C']]
'''

some_str = list('doGCat')
upper_letter = []
for i in some_str:
    if i.isupper():
        upper_letter.append(i)
    else:
        pass

if len(upper_letter) > 0:
    print([True, upper_letter])
else:
    print([False, upper_letter])

'''
Сложите все числа в списке, они могут быть отрицательными,
если список пустой вернуть 0
[] == 0
[1, 2, 3] == 6
[1.1, 2.2, 3.3] == 6.6
[4, 5, 6] == 15
range(101) == 5050
'''

some_digit_list = [i for i in range(101)]
sum_ = 0
for i in some_digit_list:
    sum_ += i
print(sum_)
