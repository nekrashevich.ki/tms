"""
создайте класс работник
У класса есть конструктор, в который надо передать имя и ЗП.
У класса есть 2 метода: получение имени сотрудника и получение
количества созданных работников. При создании экземпляра класса
должен вызываться метод, который отвечает за увеличение количества
работников на 1.
"""


class Employee:
    count = 0

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        Employee.increase_count_of_employee()

    @classmethod
    def get_count_of_employees(cls):
        return cls.count

    def get_name(self):
        print(f'This is {self.name}')

    @classmethod
    def increase_count_of_employee(cls):
        cls.count += 1


empl1 = Employee('Fox', 1000)
print(empl1.get_count_of_employees())

empl2 = Employee('Auf', 4000)
print(empl2.get_count_of_employees())

empl3 = Employee('Piu', 5000)
print(empl3.get_count_of_employees())

"""
Путешествие
Вы идете в путешествие, надо подсчитать сколько у денег у каждого
студента. Класс студен описан следующим образом:

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

Необходимо понять у кого больше всего денег и вернуть его имя. Если
у студентов денег поровну вернуть: “all”.
(P.S. метод подсчета не должен быть частью класса)


assert most_money([ivan, oleg, sergei]) == "all"
assert most_money([ivan, oleg]) == "ivan"
assert most_money([oleg]) == "Oleg"
"""


class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money


def most_money(lst: list):
    money_lst = [i.money for i in lst]
    max_value = max(money_lst)
    if money_lst.count(max_value) == len(money_lst) and len(money_lst) > 1:
        return 'all'
    else:
        for person in lst:
            if person.money == max_value:
                return person.name


ivan = Student('Ivan', 4000)
oleg = Student('Oleg', 2000)
sergei = Student('Sergei', 3000)
alexey = Student('Alexey', 3000)

# print(most_money([ivan, oleg, sergei]))
# print(most_money([ivan, oleg]))
# print(most_money([oleg]))

# assert most_money([ivan, oleg, sergei]) == "all"
assert most_money([sergei, alexey]) == "all"
assert most_money([ivan, oleg]) == "Ivan"
assert most_money([oleg]) == "Oleg"
