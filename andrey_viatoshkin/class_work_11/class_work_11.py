from abc import ABC, abstractmethod
# 1) Создать абстрактным класс Pet с абстрактным методом move().
# Унаследуйте от класса Pet класс Parrot и переопределите в нем метод
# move().


class Pet(ABC):
    @abstractmethod
    def move(self):
        print('Pet can move')


class Parrot(Pet):
    def move(self):
        print('Parrot can move')


parrot_1 = Parrot()
parrot_1.move()

# 2 Реализовать следующую струкуру


class Animal(ABC):
    @abstractmethod
    def move(self):
        print('Animal can move')

    @abstractmethod
    def sleep(self):
        print('Animal can sleep')

    @abstractmethod
    def eat(self):
        print('Animal can eat')

    @abstractmethod
    def drink(self):
        print('Animal can drink')


class Pet1(Animal):
    @abstractmethod
    def love(self):
        print('Pet1 can love')

    @abstractmethod
    def eat_slippers(self):
        print('Pet1 can eat slippers')


class WildAnimal(Animal):

    @abstractmethod
    def hunt(self):
        print('WildAnimal can hunt')

    @abstractmethod
    def dig_hole(self):
        print('WildAnimal can dig hole')

    @abstractmethod
    def roar(self):
        print('WildAnimal can roar')


class Cat(Pet1):

    def move(self):
        print('Cat can move')

    def sleep(self):
        print('Cat can sleep')

    def eat(self):
        print('Cat can eat')

    def drink(self):
        print('Cat can drink')

    def love(self):
        print('Cat can love')

    def eat_slippers(self):
        print('Cat can eat slippers')


class Dog(Pet1):
    def move(self):
        print('Dog can move')

    def sleep(self):
        print('Dog can sleep')

    def eat(self):
        print('Dog can eat')

    def drink(self):
        print('Dog can drink')

    def love(self):
        print('Dog can love')

    def eat_slippers(self):
        print('Dog can eat slippers')


class Lion(WildAnimal):

    def move(self):
        print('Lion can move')

    def sleep(self):
        print('Lion can sleep')

    def eat(self):
        print('Lion can eat')

    def drink(self):
        print('Lion can drink')

    def hunt(self):
        print('Lion can hunt')

    def dig_hole(self):
        print('Lion can dig hole')

    def roar(self):
        print('Lion can roar')


class Wolf(WildAnimal):

    def move(self):
        print('Wolf can move')

    def sleep(self):
        print('Wolf can sleep')

    def eat(self):
        print('Wolf can eat')

    def drink(self):
        print('Wolf can drink')

    def hunt(self):
        print('Wolf can hunt')

    def dig_hole(self):
        print('Wolf can dig hole')

    def roar(self):
        print('Wolf can roar')


wolf_1 = Wolf()
wolf_1.roar()
cat_1 = Cat()
cat_1.love()
dog_1 = Dog()
dog_1.eat()
lion_1 = Lion()
lion_1.dig_hole()


# 3 Создать класс Book. Атрибуты: количество страниц, год издания, автор,
# цена. Добавить валидацию в конструкторе на ввод корректных данных


class Book:
    def __init__(self, pages, year, author, price):
        self.pages = self.validate_pages(pages)
        self.year = self.validate_year(year)
        self.author = self.validate_author(author)
        self.price = self.validate_price(price)

    @staticmethod
    def validate_pages(pages):
        if pages < 4000:
            return pages
        raise PagesException

    @staticmethod
    def validate_year(year):
        if year > 1980:
            return year
        raise YearException

    @staticmethod
    def validate_author(author):
        if author.isalpha():
            return author
        raise AuthorException

    @staticmethod
    def validate_price(price):
        if price > 100 and price < 10000:
            return price
        raise PriceException


class PagesException(Exception):
    def __init__(self, message='количество страниц не должно быть'
                               'больше 4000'):
        super().__init__(message)


class YearException(Exception):
    def __init__(self, message='год издания дложен быть больше 1980'):
        super().__init__(message)


class AuthorException(Exception):
    def __init__(self, message='имя автора должно содержать только буквы'):
        super().__init__(message)


class PriceException(Exception):
    def __init__(self, message='цена должна быть от 100 до 10000 '):
        super().__init__(message)


book_1 = Book(3999, 2000, 'Pelevin', 9999)


# Создать lambda функцию, которая принимает на вход имя и выводит его в
# формате “Hello, {name}”
# func_name = lambda x: f'Hello, {x}'
# hello_name = func_name('Andrey')


# Создать lambda функцию, которая принимает на вход список имен и выводит
# их в формате “Hello, {name}” в другой список
list_of_names = ['Andrey', 'Ivank']
func_name_list = list(map(lambda name: f'Hello, {name}', list_of_names))
print(func_name_list)


# func_name_list('Andrey', 'Ivan')

# Дан список чисел. Вернуть список, где каждый число переведен
# о в строку [5, 3] -> [‘5’, ‘3’]

convert_to_str = list(map(lambda x: str(x), [1, 2, 3]))
print(convert_to_str)


# Дан список имен, отфильтровать все имена, где есть буква k
filtered_list_of_names = list(filter(lambda x: 'k' in x, list_of_names))
print(filtered_list_of_names)

# s = {"a": 10, "b": 20, "c": 0, "d": -1}
# отсортировать словарь с помощью функции sorted()
s = {"a": 10, "b": 20, "c": 0, "d": -1}
sorted_dict = dict(sorted(s.items(), key=lambda item: item[1]))
print(sorted_dict)
