# Библиотека
# Создайте класс book с именем книги, автором, кол-м страниц,
# ISBN, флагом, зарезервирована ли книги или нет. Создайте класс
# пользователь который может брать книгу, возвращать, бронировать.
# Если другой пользователь хочет взять зарезервированную книгу
# (или которую уже кто-то читает - надо ему про это сказать).
class Book:
    def __init__(self, name: str, author: str, count_of_pages: int, isbn: str,
                 available=True):
        self.name = name
        self.author = author
        self.count_of_pages = count_of_pages
        self.isbn = isbn
        self.is_available = available
        self.reserved = []


class User:
    def __init__(self, name: str):
        self.name = name
        self.book_reads_name = []

    def get_book(self, book: Book):
        # Здесь такая логика: сначала бронируй, потом бери.
        # Не забронировав книгу, нельзя её взять
        if book.is_available and (self.name in book.reserved or
                                  len(book.reserved) == 0):
            book.is_available = False
            if self.name in book.reserved:
                book.reserved.remove(self.name)
            self.book_reads_name.append(book.name)
            print(f"{self.name}, You can read it now.")
        elif book.name in self.book_reads_name:
            print(f"{self.name}, You already have this book")
        elif self.name in book.reserved:
            print(f"{self.name}, This book is not available now, try later")
        else:
            print(f"{self.name}, This book is not available, but you can "
                  f"book it.")

    def return_book(self, book: Book):
        if not book.is_available and book.name in self.book_reads_name:
            book.is_available = True
            self.book_reads_name.remove(book.name)
            print(f"{self.name}, Thanks for returning this book.")
        else:
            print(f"{self.name}, You haven\'t this book for returning")

    def booking_book(self, book: Book):
        if self.name not in book.reserved:
            book.reserved.append(self.name)
            print(f"{self.name}, This book is now booked by you")
        else:
            print(f"{self.name}, You have already booked this book")


# Банковский вклад
# Создайте класс инвестиция. Который содержит необходимые поля и методы,
# например сумма инвестиция и его срок.
# Пользователь делает инвестиция в размере N рублей сроком на R лет под 10%
# годовых (инвестиция с возможностью ежемесячной капитализации - это означает,
# что проценты прибавляются к сумме инвестиции ежемесячно).
# Написать класс Bank, метод deposit принимает аргументы N и R, и возвращает
# сумму, которая будет на счету пользователя.
class Investment:
    def __init__(self, n: float, r: int):
        """
        :param n: roubles
        :param r: years
        """
        self.money = n
        self.time = r


class Bank(Investment):
    def deposit(self):
        self.money = self.money * (1 + 0.1 / 12)**(self.time * 12)
        return self.money


if __name__ == "__main__":

    Harry_Potter = Book('Harry Potter and philosophy stone',
                        'J.K.Rowling', 320, '978-3-16-148410-0')
    Pushkins_tails = Book('Pushkin\'s fairy tails', 'A.S.Pushkin',
                          153, '187-6-19-142340-1')
    The_memoirs_of_Sherlock = Book('The Memoirs of Sherlock Holmes',
                                   'Arthur Conan Doyle', 384,
                                   '324-5-63-346146-5', available=False)
    Coraline = Book('Coraline', 'Neil Gaiman', 319, '784-9-05-049576-51')
    Alex = User('Alex')
    John = User('John')
    Tom = User('Tom')

    Alex.booking_book(Harry_Potter)
    Alex.get_book(Harry_Potter)
    Alex.return_book(Harry_Potter)
    John.booking_book(Harry_Potter)
    John.get_book(Harry_Potter)
    Alex.booking_book(Pushkins_tails)
    John.booking_book(The_memoirs_of_Sherlock)
    John.get_book(Harry_Potter)
    Tom.get_book(Pushkins_tails)
    Tom.booking_book(Pushkins_tails)
    Tom.get_book(Pushkins_tails)
    Tom.return_book(Pushkins_tails)

    print("-" * 20)

    user = Bank(2000, 2)
    print(round(user.deposit(), 2))
