import decorators_8


# 1. Функция декоратор, который печатает перед и после вызова функции слова
# “Before” and “After”
@decorators_8.dec_before_after
def abc(string: str):
    """
    This function prints 'Hello again!'
    """
    return string


# 2. Функция декоратор, которая добавляет 1 к заданному числу функции
@decorators_8.dec_increment
def second_task(x: int):
    """
    This function takes one argument and returns it.
    """
    return x


# 3. Функция декоратор, которая переводит полученный текст в верхний регистр
@decorators_8.dec_upper
def third_task(x: str):
    """
    This function takes one argument and returns it.
    """
    return x


# 4. Функция декоратор func_name, которая выводит на экран имя
# функции (func.__name__)
@decorators_8.func_name
def fourth_task(a: int):
    """
    This function takes one argument and returns it.
    """
    return a


# 5. Напишите декоратор change, который делает так, что задекорированная
# функция принимает все свои не именованные аргументы в порядке, обратном
# тому, в котором их передали
@decorators_8.change
def fifth_task(*args):
    """
    This function takes one argument and returns it.
    """
    return args


# 6. Напишите декоратор, который вычисляет время работы декорируемой функции
# (timer)
@decorators_8.timer
def check_task(arg):
    """
    This function takes one argument and returns it. There is a rubbish
    inside function to make it more difficult and check the speed of a
    processor.
    """
    k = 0
    for i in range(10000000):
        k += i
    return arg


# 7. Напишите функцию, которая вычисляет значение числа Фибоначчи для заданного
# количество элементов последовательности и возвращает его и оберните ее
# декоратором timer и func_name
@decorators_8.timer
@decorators_8.func_name
def fib(arg):
    a = 0
    b = 1
    arg = arg - 2
    for i in range(arg):
        a, b = b, a + b
    return b


# 8. Напишите функцию, которая вычисляет сложное математическое выражение и
# оберните ее декоратором из пункта 1, 2
@decorators_8.dec_before_after
@decorators_8.dec_increment
def difficult(x):
    """
    This function takes an argument and make some mathematic operations with
    it,prints the result and returns it.
    """
    b = x ** 3 - x + x ** x
    print(b)
    return b


# 9. Напишите декоратор, который проверял бы тип параметров функции,
# конвертировал их если надо и складывал:
#
# @typed(type=’str’)
# def add_two_symbols(a, b):
#     return a + b
#
# add_two_symbols("3", 5) -> "35"
# add_two_symbols(5, 5) -> "55"
# add_two_symbols('a', 'b') -> 'ab’
#
# @typed(type=’int’)
# def add_three_symbols(a, b, с):
#     return a + b + с
#
# add_three_symbols(5, 6, 7) -> 18
# add_three_symbols("3", 5, 0) -> 8
# add_three_symbols(0.1, 0.2, 0.4) -> 0.7000000000000001
@decorators_8.tiped("str")
def last_task(*args):
    """
    This function takes arguments and just returns them.
    """
    return args


if __name__ == "__main__":
    while True:
        print('Choose the function to run: ',
              '1)Функция с декоратором, который печатает перед и после'
              ' вызова функции слова Before и After',
              '2)Функция с  декоратором, который добавляет 1 к заданному'
              ' числу',
              '3)Функция с  декоратором, который переводит полученный текст'
              ' в верхний регистр',
              '4)Функция с  декоратором, который  выводит на экран имя'
              ' функции',
              '5)Функция с  декоратором, который делает так, '
              'что задекорированная функция принимает все свои не '
              'именованные '
              'аргументы в порядке, обратном тому, в котором их передали',
              '6)Функция с  декоратором, который вычисляет время работы'
              ' декорируемой функции',
              '7)Функция, которая вычисляет значение числа Фибоначчи для '
              'заданного количествa элементов последовательности и '
              'возвращает его, с декораторами timer и func_name',
              '8)Функция, которая вычисляет сложное математическое выражение'
              ' с декораторами из пункта 1, 2',
              '9)Функция с  декоратором, который проверял бы тип параметров '
              'функции,конвертировал их, если надо, и складывал', sep='\n'
              )
        menu_choice = input()
        if menu_choice not in ['1', '2', '3', '4', '5', '6', '7', '8', '9']:
            print('Try again')
            continue
        else:
            break

    function_params_mapper = {
        "1": {"function": abc, "params": "Hello"},
        "2": {"function": second_task, "params": 10},
        "3": {"function": third_task, "params": 'hello again'},
        "4": {"function": fourth_task, "params": 5},
        "5": {"function": fifth_task, "params": (1, 2, 3)},
        "6": {"function": check_task, "params": 5},
        "7": {"function": fib, "params": 20},
        "8": {"function": difficult, "params": 5},
        "9": {"function": last_task, "params": (1, "a", 2, "b")}
    }

    for key in function_params_mapper.keys():
        if key == menu_choice:
            func = function_params_mapper.get(key).get("function")
            params = function_params_mapper.get(key).get("params")
            if type(params) == tuple:
                print(func(*params))
            else:
                print(func(params))
            break
