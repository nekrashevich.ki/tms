from random import randint

# 1. Быки и коровы
# В классическом варианте игра рассчитана на двух игроков. Каждый из игроков
# задумывает и записывает тайное 4-значное число с неповторяющимися цифрами.
# Игрок, который начинает игру по жребию, делает первую попытку отгадать
# число. Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое
# противнику. Противник сообщает в ответ, сколько цифр угадано без совпадения
# с их позициями в тайном числе (то есть количество коров) и сколько угадано
# вплоть до позиции в тайном числе (то есть количество быков).
# При игре против компьютера игрок вводит комбинации одну за другой, пока не
# отгадает всю последовательность.
# Ваша задача реализовать программу, против которой можно сыграть в
# "Быки и коровы"
print("Task 1:")
answer_number = ""
for i in range(4):
    random_number = randint(0, 9)
    while answer_number.find(str(random_number)) != -1:
        random_number = randint(0, 9)
    answer_number += str(random_number)
print(answer_number)
while True:
    number = input("Введите 4-значное число с неповторяющимися цифрами: ")
    if len(number) != 4:
        continue
    bull = 0
    cow = 0
    for i in range(4):
        if answer_number[i] == number[i]:
            bull += 1
        if number[i] in answer_number:
            cow += 1
    print(f"{cow} коров(а/ы), {bull} бык(а)")
    if bull == 4:
        print('Вы победили!')
        break


# 2. Like
# Создайте программу, которая, принимая массив имён, возвращает строку,
# описывающую количество лайков (как в Facebook).
print("Task 2:")
names_input = input("Введите имена через запятую: ")
names = names_input.split()
if len(names_input) == 0:
    print("No one likes this")
for i in names:
    if len(names) == 1:
        print(names[0].replace(",", ""), "like this")
    elif len(names) == 2:
        print(names[0].replace(",", ""), "and", names[1].replace(",", ""),
              "like this")
    elif len(names) == 3:
        print(names[0], names[1].replace(",", ""), "and",
              names[2].replace(",", ""), "like this")
    elif len(names) > 3:
        print(names[0], names[1].replace(",", ""), "and 2 others like this")


# 3. Напишите программу, которая перебирает последовательность от 1 до 100.
# Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа, а для
# чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5 надо
# печатать "FuzzBuzz". Иначе печатать число.
print("Task 3:")
for i in range(1, 101):
    if i % 5 == 0 and i % 3 == 0:
        print("FuzzBuzz")
    elif i % 3 == 0:
        print("Fuzz")
    elif i % 5 == 0:
        print("Buzz")
    else:
        print(i)


# 4. Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет “:” и пробел
# [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]
print("Task 4:")
lst = ["a", "b", "c"]
for i, j in enumerate(lst, 1):
    lst[i - 1] = f"{i}: " + lst[i - 1]
print(lst)


# 5. Проверить, все ли элементы одинаковые
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True
print("Task 5:")
elements = [1, 1, 1]
print(len(set(elements)) == 1)


# 6. Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]
print("Task 6:")
string = input("Enter a string: ")
upper_letters = []
for letter in string:
    if letter.isupper():
        upper_letters += letter
print([len(upper_letters) == 0, upper_letters])


# 7. Сложите все числа в списке, они могут быть отрицательными, если список
# пустой вернуть 0
# [] == 0
# [1, 2, 3] == 6
# [1.1, 2.2, 3.3] == 6.6
# [4, 5, 6] == 15
# range(101) == 5050
print("Task 7:")
array = [1, 2, 3]
print(sum(array))
array = range(101)
print(sum(array))
