from math import pi

print('IF')

# 1. Если значение некой переменной больше 0, выводилось бы специальное
# сообщение.Один раз выполните программу при значении переменной больше 0,
# второй раз — меньше 0.
print('Task 1:', end=' ')
x = 5
if x > 0:
    print("This number is > 0")


# 2. Усовершенствуйте предыдущий код с помощью ветки else так, чтобы в
# зависимости от значения переменной, выводилась либо 1, либо -1.
print('Task 2:', end=' ')
if x > 0:
    print(1)
else:
    print(-1)


# 3. Самостоятельно придумайте программу, в которой бы использовалась
# инструкция if (желательно с веткой elif). Вложенный код должен содержать не
# менее трех выражений.
print('Task 3:', end=' ')
password = "thegoodpassworld"
if len(password) < 8:
    print("This password is too short")
elif len(password) < 13:
    print("This password is normal")
else:
    print("This password is good")


print("ELIF")

# 1. Напишите программу по следующему описанию:
# a. двум переменным присваиваются числовые значения;
# b. если значение первой переменной больше второй, то найти разницу значений
# переменных (вычесть из первой вторую), результат связать с третьей
# переменной;
# c. если первая переменная имеет меньшее значение, чем вторая, то третью
# переменную связать с результатом суммы значениий двух первых переменных;
# d. во всех остальных случаях, присвоить третьеий переменной значение первой
# переменной;
# e. вывести значение третьей переменной на экран.
print('Task 1:', end=' ')
a, b = 3, 5
if a > b:
    c = a - b
elif a < b:
    c = a + b
else:
    c = a
print(c)

# 2. Придумайте программу, в которой бы использовалась инструкция
# if-elif-else. Количество ветвей должно быть как минимум четыре.
print('Task 2:', end=' ')
angle = 0.1
angle = abs(angle)
if (angle >= 0) and (angle < pi / 2):
    print("Acute angle")
elif angle == pi / 2:
    print("Right angle")
elif (angle >= pi / 2) and (angle < pi):
    print("Obtuse angle")
elif angle == pi:
    print("Straight angle")
elif (angle > pi) and (angle < 2 * pi):
    print("Reflex angle")
else:
    print("Angle >= than 2*PI")


print("FOR")

# 1. Создайте список, состоящий из четырех строк. Затем, с помощью цикла for,
# выведите строки поочередно на экран.
print("Task 1:")
lst_1 = ["Hello", "I'", "m", "Aleksey"]
for i in lst_1:
    print(i)


# 2. Измените предыдущую программу так, чтобы в конце каждой буквы строки
# добавлялось тире. (Подсказка: цикл for может быть вложен в другой цикл.)
print("Task 2:")
lst_1 = ["Hello", "I'", "m", "Aleksey"]
for i in lst_1:
    print(i, end="-\n")


# 3. Создайте список, содержащий элементы целочисленного типа, затем с
# помощью цикла перебора измените тип данных элементов на числа с плавающей
# точкой. (Подсказка: используйте встроенную функцию float().)
print("Task 3:")
lst_2 = [1, 2, 3, 4]
for i in lst_2:
    i = float(i)
    print(i)


# 4. Есть два списка с одинаковым количеством элементов, в каждом из них этих
# списков есть элемент “hello”. В первом списке индекс этого элемента 1, во
# втором 7. Одинаковых элементов в обоих списках может быть несколько. Напишите
# программу которая распечатает индекс слова “hello” в первом списке и
# условный номер списка, индекс слова во втором списки и условный номер
# списка со словом Совпадают. Например, программа выведет на экран:
# “Совпадают 1-й элемент из первого списка и 7-й элемент из второго списка”
print("Task 4:")
lst_3 = ["joker", "hello", "dark", "keyboard", "phantom", "apple", "chocolate",
         "jewels"]
lst_4 = ["trigger", "finger", "run", "chocolate", "printer", "mouse", "chair",
         "hello"]
for i in range(len(lst_3)):
    for j in range(len(lst_4)):
        if lst_3[i] == lst_4[j]:
            print(f"Совпадают {i + 1}-й элемент из первого списка и {j + 1}-й "
                  f"элемент из второго списка")


print("WHILE")


# 1. Напишите скрипт на языке программирования Python, выводящий ряд чисел
# Фибоначчи (числовая последоватьность в которой числа начинаются с 1 и 1 или
# же и 0 и 1, пример: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, ...).
# Запустите его на выполнение. Затем измените код так, чтобы выводился ряд
# чисел Фибоначчи, начиная с пятого члена ряда и заканчивая двадцатым.
print("Task 1:")
n = 20
past_numb = 0
present_numb = 1
counter = 1
while counter < n:
    if counter > 3:
        print(present_numb, end=" ")
    counter += 1
    present_numb += past_numb
    past_numb = present_numb - past_numb
else:
    print()


# 2. Напишите цикл, выводящий ряд четных чисел от 0 до 20. Затем, каждое
# третье число в ряде от -1 до -21.
print("Task 2:")
i = 0
while i < 21:
    if i % 2 == 0:
        print(i, end=" ")
    i += 1
i = -1
print("")
while i > -22:
    if i % 3 == 2:
        print(i, end=" ")
    i -= 1
print()


# 3. Самостоятельно придумайте программу на Python, в которой бы
# использовался цикл while. В комментарии опишите что она делает.

# Проверка пароля
print("Task 3:")
password = input("Enter your password:")
while password != "abc":
    print("Wrong password")
    password = input("Enter your password:")
print("Access allowed")


print("INPUT")

# 1. Напишите программу, которая запрашивает у пользователя:
# - его имя (например, "What is your name?")
# - возраст ("How old are you?")
# - место жительства ("Where are you live?")
# После этого выводила бы три строки:
# "This is имя"
# "It is возраст"
# "(S)he lives in место_жительства"
print("Task 1:")
name = input("What is your name?:")
age = input("How old are you?:")
place = input("Where are you live?:")
print("This is", name)
print("It is", age)
print("(S)he lives in", place)


# 2. Напишите программу, которая предлагала бы пользователю решить пример
# 4 * 100 - 54. Потом выводила бы на экран правильный ответ и ответ
# пользователя. Подумайте, нужно ли здесь преобразовывать строку в число.
print("Task 2:")
your_answer = input("4 * 100 - 54 =")
correct_answer = 4 * 100 - 54
print(f"Your answer = {your_answer}, the right one = {correct_answer}")


# 3. Запросите у пользователя четыре числа. Отдельно сложите первые два и
# отдельно вторые два. Разделите первую сумму на вторую. Выведите результат
# на экран так, чтобы ответ содержал две цифры после запятой.
print("Task 3:")
number_1 = float(input("Enter number 1: "))
number_2 = float(input("Enter number 2: "))
number_3 = float(input("Enter number 3: "))
number_4 = float(input("Enter number 4: "))
sum_1 = number_1 + number_2
sum_2 = number_3 + number_4
quotient = sum_1 / sum_2
print('%.2f' % quotient)


print("Доп. задания:")

# 1. Из заданной строки получить нужную строку:
# "String" => "SSttrriinngg"
# "Hello World" => "HHeelllloo  WWoorrlldd"
# "1234!_ " => "11223344!!__  "
print("Task 1:")
string = "String"
new_string = ""
for i in string:
    new_string += i * 2
print(new_string)


# 2. Если х в квадрате больше 1000 - пишем "Hot" иначе - "Nope"
# '50' == "Hot"
# 4 == "Nope"
# "12" == "Nope"
# 60 == "Hot"
print("Task 2:")
x = input("x = ")
if int(x) ** 2 > 1000:
    print("Hot")
else:
    print("Nope")


# 3. Сложите все числа в списке, они могут быть отрицательными, если список
# пустой вернуть 0
# [] == 0
# [1, 2, 3] == 6
# [1.1, 2.2, 3.3] == 6.6
# [4, 5, 6] == 15
# range(101) == 5050
print("Task 3:")
array = [1, 2, 3]
print(sum(array))
array = range(101)
print(sum(array))


# 4. Подсчет букв
# Дано строка и буква => "fizbbbuz","b", нужно подсчитать сколько раз буква b
# встречается в заданной строке 3
# "Hello there", "e" == 3
# "Hello there", "t" == 1
# "Hello there", "h" == 2
# "Hello there", "L" == 2
# "Hello there", " " == 1
print("Task 4:")
string = "fizbbbuz"
print(string.count('b'))


# 5. Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет : и пробел
# [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]
print("Task 5:")
lst = ["a", "b", "c"]
for i, j in enumerate(lst, 1):
    lst[i - 1] = f"{i}: " + lst[i - 1]
print(lst)


# 6. Напишите программу, которая по данному числу n от 1 до n выводит
# на экран n флагов. Изображение одного флага имеет размер 4×4 символов.
# Внутри каждого флага должен быть записан его номер — число от 1 до n.
print("Task 6:")
n = int(input("Enter n: "))
for i in range(n):
    print("+___", end=" ")
print()
for i in range(n):
    print(f"|{i + 1} /", end=" ")
print()
for i in range(n):
    print("|__\\", end=" ")
print()
for i in range(n):
    print("|   ", end=" ")
print()


# 7. Напишите программу. Есть 2 переменные salary и bonus. Salary - integer,
# bonus - boolean.
# Если bonus - true, salary должна быть умножена на 10. Если false - нет
# 10000, True == '$100000'
# 25000, True == '$250000'
# 10000, False == '$10000'
# 60000, False == '$60000'
# 2, True == '$20'
# 78, False == '$78'
# 67890, True == '$678900'
print("Task 7:")
salary = 50
bonus = True
print(f"${salary * 10 * bonus}")


# 8. Проверить, все ли элементы одинаковые
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True
print("Task 8:")
elements = [1, 1, 1]
print(len(set(elements)) == 1)


# 9. Cуммирование. Необходимо подсчитать сумму всех чисел до заданного:
# дано число 2, результат будет -> 3(1+2)
# дано число 8, результат будет -> 36(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)
# 1 == 1
# 8 == 36
# 22 == 253
# 100 == 5050
print("Task 9:")
number = int(input("Enter a number: "))
my_sum = 0
if number != 0:
    for i in range(1, number + 1):
        my_sum += i
    print(my_sum)


# 10. Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]
print("Task 10:")
string = input("Enter a string: ")
upper_letters = []
for letter in string:
    if letter.isupper():
        upper_letters += letter
print([len(upper_letters) == 0, upper_letters])
