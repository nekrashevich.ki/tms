# сторона куба
a = 3
# мат. формулы
cube_vol = a**3
cube_side_surf = 4 * a**2
# вывод на экран результата
print(f"cube edge = {a}")
print(f"cube volume = {cube_vol}")
print(f"cube side surface = {cube_side_surf}")
