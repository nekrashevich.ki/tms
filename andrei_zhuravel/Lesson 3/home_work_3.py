from math import sqrt
import random


# Task 1. Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
str1 = 'www.my_site.com#about'
print('Task 1. : ', str1.replace('#', '/'))


# Task 2. В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"
str2 = 'Ivanou Ivan'
a = str2.split(' ')
b = a[::-1]
print('Task 2. : ', ' '.join(b))


# Task 3. Напишите программу которая удаляет пробел в начале строки
str3 = ' wertigo'
print('Task 3. : ', str3.lstrip(' '))


# Task 4. Напишите программу которая удаляет пробел в конце строки
str4 = 'wertigo '
print('Task 4. : ', str4.rstrip(' '))


# Task 5. a = 10, b=23, поменять значения местами,
# чтобы в переменную “a” было записано значение “23”, в “b” - значение “-10”
a = 10
b = 23
a, b = b, -a
print('Task 5. : ', a, b)


# Task 6. значение переменной “a” увеличить в 3 раза,
# а значение “b” уменьшить на 3
# Использую переменные из task 5.
a = a * 3
b = b - 3
print('Task 6. : ', a, b)


# Task 7. преобразовать значение “a” из целочисленного в число
# с плавающей точкой (float), а значение в переменной “b” в строку
a = float(a)
b = str(b)
print('Task 7. : ', type(a), a, type(b), b)


# Task 8. Разделить значение в переменной “a” на 11
# и вывести результат с точностью 3 знака после запятой
print('Task 8. : ', round(a / 11, 3))


# Task 9. Преобразовать значение переменной “b” в число с плавающей точкой
# и записать в переменную “c”. Возвести полученное число в 3-ю степень.
c = float(b)
print('Task 9. : ', c**3)


# Task 10. Получить случайное число, кратное 3-м
w = random.randrange(0, 100, 3)
print('Task 10. : ', w)


# Task 11. Получить квадратный корень из 100 и возвести в 4 степень
g = int(pow(sqrt(100), 4))
print('Task 11. : ', g)


# Task 12. Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
# “Hi guysHi guysHi guysToday”
str12 = 'Hi guys' * 3 + 'Today'
print('Task 12. : ', str12)


# Task 13. Получить длину строки из предыдущего задания
print('Task 13. : ', len(str12))


# Task 14. Взять предыдущую строку и вывести слово “Today”
# в прямом и обратном порядке
print('Task 14. : ', str12[-5:], ' ', str12[:-6:-1])


# Task 15. “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву
# в прямом и обратном порядке
str15 = 'Hi guysHi guysHi guysToday'
str15_1 = str15.split()
str15_2 = ''.join(str15_1)
print('Task 15. : ', str15_2[1::2], ' ', str15_2[-2::-2])


# Task 16. Используя форматирования подставить результаты из задания 10 и 11
# в следующую строку
# “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>, <в обратном>”
w = str(w)
g = str(g)
print('Task 16. : ', f"Task 10: {w}, {w[::-1]}, Task 11: {g}, {g[::-1]}")


# Task 17. Есть строка: “my name is name”. Напечатайте ее,
# но вместо 2ого “name” вставьте ваше имя.
str17 = 'my name is name'
print('Task 17. : ', str17.replace('is name', 'is Andrei'))


# Task 18. Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
# б) все слова в нижнем регистре
# в) все слова в верхнем регистре
print('Task 18.a : ', str12.title())
print('Task 18.б : ', str12.lower())
print('Task 18.в : ', str12.upper())


# Task 19. Посчитать сколько раз слово “Task”
# встречается в строке из задания 12
print('Task 19 : ', str12.count('Task'))
