"""
1) Напишите функцию, которая возвращает строку: “Hello world!”
"""


def hello_world():
    print('Hello world')
    return 1


a = hello_world()
print(a)
"""
2) Напишите функцию, которая вычисляет сумму трех чисел и возвращает
результат в основную ветку программы.
"""


def f(x, y, z):
    return x + y + z


a = f(1, 2, 3)
print(a)


"""
3) Придумайте программу, в которой из одной функции вызывается вторая.
При этом ни одна из них ничего не возвращает в основную ветку программы,
обе должны выводить результаты своей работы с помощью функции print().
"""


def fun1():
    x = input("enter text: ")

    def fun2():
        print(x)
    fun2()


fun1()


"""
4) Напишите функцию, которая не принимает отрицательные числа.
и возвращает число наоборот.
21445 => 54421
123456789 => 987654321
"""
num = int(input("numbers: "))


def numb(num):
    if num <= 0:
        print("None False")
    else:
        num = str(num)
    return num[::-1]


print(numb(num))


"""
5) Напишите функцию fib(n), которая по данному целому неотрицательному n
возвращает n-e число Фибоначчи.
"""


def fib(n):
    if n in (1, 2):
        return 1
    return fib(n - 1) + fib(n - 2)


print(fib(10))


"""
6) Напишите функцию, которая проверяет на то, является ли строка
палиндромом или нет. Палиндром — это слово или фраза, которые одинаково
читаются слева направо и справа налево.
"""
string = input("enter string: ")


def string_pol():
    if string == string[::-1]:
        print(string, "- string pol")
    else:
        print(string, "- string no pol")


string_pol()


"""
7) У вас интернет магазин, надо написать функцию которая проверяет что
введен правильный купон и он еще действителен

def check_coupon(entered_code, correct_code, current_date, expiration_date):
    #Code here!

check_сoupon("123", "123", "July 9, 2015", "July 9, 2015")  == True
check_сoupon("123", "123", "July 9, 2015", "July 2, 2015")  == False
"""
entered_code = input("entered_code: ")
correct_code = input("correct_code: ")
current_date = input("current_date: ")
expiration_date = input("expiration_date: ")


def check_coupon(entered_code, correct_code,
                 current_date, expiration_date):
    coupon_data1 = ["123", "123", "July 9, 2015", "July 9, 2015"]
    coupon_data2 = [entered_code] + [correct_code] + [current_date] + \
                   [expiration_date]

    if coupon_data1 == coupon_data2:
        print(coupon_data1, True)
    else:
        print(coupon_data2, False)


check_coupon(entered_code, correct_code, current_date, expiration_date)


"""
8) Фильтр. Функция принимает на вход список, проверяет есть ли эти
элементы в списке exclude, если есть удаляет их и возвращает список с
оставшимися элементами
exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]

filter_list(["Mallard", "Hook Bill", "African", "Crested", "Pilgrim",
"Toulouse", "Blue Swedish"]) => ["Mallard", "Hook Bill", "Crested",
"Blue Swedish"]
["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"] =>
["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"]
["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"] => []
"""
filter_list = ["Mallard", "Hook Bill", "African", "Crested",
               "Pilgrim", "Toulouse", "Blue Swedish"]


def filter_ex(filter_list):
    exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
    return False if filter_list in exclude else True


filtered = filter(filter_ex, filter_list)
vol = tuple(filtered)
print(vol)
