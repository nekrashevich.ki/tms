"""
Быки и коровы
В классическом варианте игра рассчитана на двух игроков. Каждый из игроков
задумывает и записывает тайное 4-значное число с неповторяющимися цифрами.
Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое
противнику. Противник сообщает в ответ, сколько цифр угадано без совпадения
с их позициями в тайном числе (то есть количество коров) и сколько угадано
вплоть до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой, пока не
отгадает всю последовательность.
Ваша задача реализовать программу, против которой можно сыграть в
"Быки и коровы"
Пример
Загадано число 3219
2310
Две коровы, один бык
3219
Вы выиграли!
"""
from random import choice
z = '0123456789'
x = choice(z[1:10])
for i in range(3):
    z = ''.join(z.split(x[i]))
    x += choice(z)
while True:
    y = input("Введите четырёхзначное число: ")
    b = 0
    c = 0
    for i in range(4):
        if x[i] == y[i]:
            b += 1
        elif y[i] in x:
            c += 1
    print(y + ' содержит ' + str(b) + ' быка и ' + str(c) + ' коровы')
    if b == 4:
        print('Вы победили')
        break


"""
Like
Создайте программу, которая, принимая массив имён, возвращает строку
описывающая количество лайков (как в Facebook).
Примеры:
Введите имена через запятую: "Ann"
-> "Ann likes this"
Введите имена через запятую: "Ann, Alex"
-> "Ann and Alex like this"
Введите имена через запятую: "Ann, Alex, Mark"
-> "Ann, Alex and Mark like this"
Введите имена через запятую: "Ann, Alex, Mark, Max"
-> "Ann, Alex and 2 others like this"
Если ничего не вводить должен быть вывод:
-> "No one likes this"
"""
name = input("enter name: ")
names = name.split()
while True:
    if not name:
        print("No one likes this")
        break
    name = names
    break
if len(names) == 1:
    print(names[0].replace(",", ""), "like this")
elif len(names) == 2:
    print(names[0].replace(",", ""), "and", names[1].replace(",", ""),
          "like this")
elif len(names) == 3:
    print(names[0], names[1].replace(",", ""), "and",
          names[2].replace(",", ""), "like this")
elif len(names) > 3:
    print(names[0], names[1].replace(",", ""), "and 2 others like this")


"""
BuzzFuzz
Напишите программу, которая перебирает последовательность от 1 до 100.
Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа,
а для чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5
надо печатать "FuzzBuzz". Иначе печатать число.
Вывод должен быть следующим:
1
2
fuzz
4
buzz
fuzz
7
8
..
"""
for num in range(1, 101):
    string = ""
    if num % 3 == 0:
        string = string + "Fuzz"
    if num % 5 == 0:
        string = string + "Buzz"
    if num % 5 != 0 and num % 3 != 0:
        string = string + str(num)
    print(string)
