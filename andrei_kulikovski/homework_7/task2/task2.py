from string import punctuation as Pun, ascii_lowercase as abc,\
    ascii_uppercase as ABC


"""
⦁	Шифр цезаря

Шифр Цезаря — один из древнейших шифров. При шифровании каждый символ
заменяется другим, отстоящим от него в алфавите на фиксированное число позиций.
Примеры:
hello world! -> khoor zruog!
this is a test string -> ymnx nx f yjxy xywnsl
Напишите две функции - encode и decode принимающие как параметр строку и
число - сдвиг.
Все взаимодействие с программой должно производиться через терминал!
Привер:
Выберите операцию:
1) Encode
2) Decode
2
Введите фразу:
И так далее!
"""
# нашел такое)
"""
import codecs

str = "alice"
print(codecs.encode(str, 'rot13'))
str = "nyvpr"
print(codecs.encode(str, 'rot13'))
"""


def coder():
    """the function uses the caesar cipher encode and decode"""
    alphabet = abc + ABC + Pun
    message = input("message: ")
    step = 3
    result1 = ''
    result2 = ''
    for i in message:
        new_vol1 = alphabet.find(i) + step
        if i in alphabet:
            result1 += alphabet[new_vol1]
    for i in result1:
        new_vol2 = alphabet.find(i) - step
        if i in alphabet:
            result2 += alphabet[new_vol2]
    print(result1)
    print(result2)


coder()
