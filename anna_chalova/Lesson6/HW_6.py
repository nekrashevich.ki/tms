from functools import reduce
# 1.Validate
n = str(input())


def luhn(n: str) -> bool:
    """
    This function validate credit card
    """
    lookup = (0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
    n = reduce(str.__add__, filter(str.isdigit, n))
    evens = sum(int(i) for i in n[-1::-2])
    odds = sum(lookup[int(i)] for i in n[-2::-2])
    return (evens + odds) % 10 == 0


if luhn(n) is True:
    print("Validation is correct")
else:
    print("Validation is NOT correct")

# 2. Подсчет количества букв


def symbol_count(word: str) -> str:
    """
    This function counts unique symbols in a word
    """
    y = []
    for i in word:
        n = i + str(word.count(i))
        if n not in y:
            y.append(n)
    y = ''.join(y)
    return y


print(symbol_count(input()))

# 3. Простейший калькулятор v0.1


def calc(n: int, a: float, b: float) -> float:
    """
    This is simple calculator
    """
    if n == 1:
        return a + b
    elif n == 2:
        return a - b
    elif n == 3:
        return a * b
    elif n == 4 and b != 0:
        return a / b
    else:
        pass


n = int(input('выберете один из вариантов: \n1) сложение\n2) вычитание\n'
              '3) умножение\n4) деление\n'))
a = float(input('Введите первое число: '))
b = float(input('Введите второе число: '))
print("Итого: ", calc(n, a, b))

# 4. Написать функцию с изменяемым числом входных параметров


def some_func(x, *args, name=None, **kwargs):
    """
    This function illustrates several kinds of different input parameters
    """
    return {'mandatory_position_argument': x,
            'additional_position_arguments': args,
            'mandatory_named_argument': {'name': name},
            'additional_named_arguments': {**kwargs}
            }


print(some_func(1, 2, 3, name='test', surname='test2', some='something'))

# 5. Работа с областями видимости

my_list = [1, 2, 3]


def add_symbol(list1: list):
    """
    This function generates new list by adding '4' to the initial list
    """
    list2 = list1.copy()
    list2.append(4)
    return list2


changed_list = add_symbol(my_list)
print("Mой список", my_list)
print('Измененный список', changed_list)

# 6. Функция проверяющая тип данных


def count_type(list1: list):
    """
    This function counts different types of elements in the list
    """
    int_count = 0
    str_count = 0
    tuple_count = 0
    for i in range(len(list1)):
        if isinstance(list1[i], int) is True:
            int_count += 1
        elif isinstance(list1[i], str) is True:
            str_count += 1
        elif isinstance(list1[i], tuple) is True:
            tuple_count += 1
    list_of_types = {'int': int_count, 'str': str_count, 'tuple': tuple_count}
    return list_of_types


initial_list = ['a', 2, 'a', (1, 2), 'b']
print(count_type(initial_list))

# 7.Написать пример чтобы hash от объекта 1 и 2 были одинаковые, а id разные
a = -9
b = -9.0
print(id(a), id(b))
print(hash(a), hash(b))

# 8. Написать функцию, которая проверяет есть ли в списке объекты,
# которые можно вызвать


def is_callable(list1: list):
    """
    This function search for callable element in list
    and returns True if any exists
    """
    for i in range(len(list1)):
        if callable(list1[i]) is True:
            return True
    return False


list_to_check = [1, 2, 3, 'a', True, 3, is_callable]
print(is_callable(list_to_check))
