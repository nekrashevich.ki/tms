import math
import random

# 1. Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
str1 = "www.my_site.com#about"
print(str1.replace("#", "/"))

# 2. В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"
str1 = "Ivanou Ivan"
list1 = str1.split()
list1.reverse()
print(' '.join(list1))

# 3. Напишите программу которая удаляет пробел в начале строки
str1 = " i love cats"
print(str1.lstrip())

# 4. Напишите программу которая удаляет пробел в конце строки
str1 = "i love cats "
if (str1.endswith(" ")):
    print(str1[:-1])
else:
    print(str1)

# 5. a = 10, b=23, поменять значения местами, чтобы в переменную “a”
# было записано значение “23”, в “b” - значение “-10”
a, b = 10, 23
a, b = b, -a
print(a, b)

# 6. Значение переменной “a” увеличить в 3 раза, значение “b” уменьшить на 3
a = 10
b = 9
a *= 3
b -= 3
b /= 3
print(a, b)

# 7. преобразовать значение “a” из целочисленного в число с плавающей точкой
# (float), а значение в переменной “b” в строку
a = 4
b = 4
a = float(a)
b = str(b)
print(a, b)

# 8. Разделить значение в переменной “a” на 11 и вывести результат с
# точностью 3 знака после запятой
a = 100
print(round(a / 11.0, 3))

# 9. Преобразовать значение переменной “b” в число с плавающей точкой
# и записать в переменную “c”. Возвести полученное число в 3-ю степень.
b = 5
b = float(b)
c = b
print(c ** 3)

# 10. Получить случайное число, кратное 3-м
rand = random.randrange(0, 1000, 3)
print(rand)
# OR
print(random.random() * 3)

# 11. Получить квадратный корень из 100 и возвести в 4 степень
num = math.sqrt(100) ** 4
print(num)

# 12. Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
# “Hi guysHi guysHi guysToday”
str1 = "Hi guys" * 3 + "Today"
print(str1)

# 13. Получить длину строки из предыдущего задания
full_len = len(str1)
print(full_len)

# 14. Взять предыдущую строку и вывести слово “Today”
# в прямом и обратном порядке
word_len = len("Today")
word = (str1[full_len - word_len:full_len])
print(word)
print(word[::-1])

# 15. “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву
# в прямом и обратном порядке
str3 = str1.replace(' ', '')
print(str3[1::2])
print(str3[-2::-2])

# 16. Используя форматирования подставить результаты из задания
# 10 и 11 в следующую строку “Task 10: <в прямом>, <в обратном>
# Task 11: <в прямом>, <в обратном>”
rand = str(rand)
num = str(num)
print("Task 10: {rand}, {rand_reverse} Task 11: {num}, {num_reverse}"
      .format(rand=rand, rand_reverse=rand[::-1],
              num=num, num_reverse=num[::-1]))

# 17. Есть строка: “my name is name”.
# Напечатайте ее, но вместо 2ого “name” вставьте ваше имя.
str2 = "my name is name"
words = str2.split()
words[-1] = "Ania"
print(' '.join(words))

# 18. Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
# б) все слова в нижнем регистре
# в) все слова в верхнем регистре
print(str1.title())
print(str1.lower())
print(str1.upper())

# 19. Посчитать сколько раз слово “Task” встречается в строке из задания 12
print(str1.count('Task'))
