from math import sqrt, pow
import random
# 10. Получить случайное число, кратное 3-м.
number = random.randrange(3, 100, 3)
print(number)

# 11. Получить квадратный корень из 100 и возвести в 4 степень.
number = pow(sqrt(100), 4)
print(number)

# 12. Строку вывести 3 раза и в конце добавить 'Today'.
s = 'Hi guys'
s = s * 3 + 'Today'
print(s)

# 13. Получить длину строки.
print(len(s))

# 14. Взять предыдущую строку и вывести 'Today' в прямом и обратном порядке.
s = 'Hi guysHi guysHi guysToday'
word_index = s.find('Today')
print(s[word_index:])
print(s[-1:word_index - 1:-1])

# 15. Вывести каждую 2-ю букву в прямом и обратном порядке.
s = s.replace(' ', '')
print(s[1::2])
print(''.join(reversed(s[1::2])))

# 16. Подставить результаты заданий 10 и 11 в следующую строку:
# “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>, <в обратном>”.
result_10 = '78'
result_11 = '10000.0'
s = f'Task 10: {result_10}, {result_10[::-1]}'
s2 = f' Task 11: {result_11}, {result_11[::-1]}'
print(s + s2)

# 17. Напечатайте строку, но вместо 2ого “name” вставьте ваше имя.
s = 'my name is name'
my_list = s.split()
my_list[3] = 'Dasha'
s = ' '.join(my_list)
print(s)

# 18.
# Каждое слово с большой буквы.
# Все слова в нижнем регистре.
# Все слова в верхнем регистре.
print(s.title())
print(s.lower())
print(s.upper())

# 19. Посчитать, сколько раз 'Task' встречается в строке из задания 12.
s = 'Hi guysHi guysHi guysToday'
count = s.count('Task')
print(count)
