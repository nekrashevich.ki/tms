s = 'abcdefghij'
# извлечь из строки первый символ, затем последний
# третий с начала, третий с конца
# измерить длину строки
print(s[0])
print(s[-1])
print(s[2])
print(s[-3])
print(len(s))

# 2
s2 = '0123456789'
# первые 8 символов
print(s2[0:8])

# 4 символа из центра строки
left_index = len(s2) // 2 - 1
right_index = len(s2) // 2
print(s2[left_index - 1:right_index + 2])

# символы с индексами кратными 3
print(s2[3::3])

# перевернутая строка
print(s2[::-1])

# 3
s3 = 'my name is name'
# напечатать, вставив свое имя вместо 2го 'name'
my_list = s3.split()
my_list[3] = 'Dasha'
print(' '.join(my_list))

# 4
test_string = 'Hello world!'
# напечатать, на каком месте буква 'w'
# кол-во букв 'l'
# начинается ли строка с подстроки 'Hello'
# заканчивается ли подстрокой 'qwe'
print(test_string.find('w'))
print(test_string.count('l'))
print(test_string.startswith('Hello'))
print(test_string.endswith('qwe'))
