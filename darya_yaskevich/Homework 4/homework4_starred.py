# 1. Напишите программу, которая будет выводить уникальное число (без пары).
a = [1, 5, 2, 9, 2, 9, 1]
b = [number for number in a if a.count(number) == 1]
print(*b)

# 2. Дан текст, содержащий различные английские буквы и знаки препинания.
# Вам необходимо найти самую частую букву в тексте.
# Результатом должна быть буква в нижнем регистре.
s = input().lower()
s2 = [i for i in s if i.isalpha()]
s2.sort()
max_count = 0
max_char = ''
for char in s2:
    if s2.count(char) > max_count:
        max_count = s2.count(char)
        max_char = char
print(max_char)
