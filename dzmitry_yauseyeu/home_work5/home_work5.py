import math
import random
from random import sample

"""Быки и коровы
В классическом варианте игра рассчитана на двух игроков.
Каждый из игроков задумывает и записывает тайное 4-значное число
с неповторяющимися цифрами. Игрок, который начинает игру по жребию,
делает первую попытку отгадать число. Попытка — это 4-значное число
с неповторяющимися цифрами, сообщаемое противнику. Противник сообщает в ответ,
сколько цифр угадано без совпадения с их позициями в тайном числе
(то есть количество коров) и сколько угадано вплоть
до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой,
пока не отгадает всю последовательность.
Ваша задача реализовать программу, против
которой можно сыграть в "Быки и коровы"
"""


# function generate digit that not contains in list_numbers
# list_numbers - list with current digits
# function returns generated digit

# secretNumberList = list()   # число, загаданное компьютером


def rand_digit(list_numbers):
    digit = 0
    while True:
        digit = random.randint(0, 9)
        if digit in list_numbers:
            continue
        else:
            break

    list_numbers.append(digit)

    return digit


""" function generate secret number with
digitCount length
digitCount - length of secret number
function returns generated secret number"""


def get_secret_number(digit_count):
    digit_list = list()  # list with generated distinct digits

    secret_number = 0
    i = 0

    while i < digit_count:

        # generate random digit
        digit = rand_digit(digit_list)
        print("current pos {}, generated digit {}".format(i, digit))

        # sum the digit into 1 number
        cur_dec_count = (math.pow(10, i))
        secret_number += digit * cur_dec_count

        i += 1

    return digit_list


def get_digitList_from_string(player_number_str):
    digit_list = list()
    for c in player_number_str:
        digit_list.append(int(c))
    return digit_list

    # i = 0
    # while True:
    #     curDecCount = (int)(math.pow(10, i))
    #     curNumber = (int)(playerNumber / curDecCount)
    #
    #     if curNumber == 0:
    #         break
    #
    #     curDigit = curNumber % 10
    #     digitList.append(curDigit)
    #     i += 1
    #
    # return digitList


def get_cows(secret_number_list, player_digit_list):
    cows_count = 0

    offset_secret = 0
    i = 0  # offset for list secretNumberList
    y = 0  # offset for list playerDigitList
    list_length = len(secret_number_list)

    i = 0
    while i < list_length:
        # function
        i += 1

    while i < list_length:

        print()
        # function
        y = 0
        while y < list_length:
            # function2
            if (i != y):
                if (secret_number_list[i] == player_digit_list[y]):
                    cows_count += 1
            y += 1

        i += 1

    for digit_secret in secret_number_list:
        offset_player = 0

        for digit_player in player_digit_list:
            if (digit_secret == digit_player) \
                    and (offset_secret != offset_player):
                cows_count += 1
            offset_player += 1

        offset_secret += 1
    return cows_count


def get_bulls(secret_number_list, player_digit_list):
    bulls_count = 0
    i = 0
    list_length = len(secret_number_list)

    while i < list_length:
        if secret_number_list[i] == player_digit_list[i]:
            bulls_count += 1
        i += 1

    return bulls_count


secret_number_list = get_secret_number(4)

# secretNumberList = getSecretNumber(9)


print("computer secret number is {}".format(secret_number_list))
print("start to play!")

while True:
    player_number_str = input()         # entered player number

    player_digit_list = get_digitList_from_string(player_number_str)

    print("player secret number is {}".format(player_digit_list))

    cows_count = get_cows(secret_number_list, player_digit_list)
    bulls_count = get_bulls(secret_number_list, player_digit_list)
    print("cowsCount is {}, bullsCount is {}".format(cows_count, bulls_count))

    if (bulls_count == 4):
        print("you won!")
        break
    print("try again!")


# second way

number = ''.join(sample('0123456789', 4))
print(number)
print('Play the "Bulls and Cows" game!')
print(number)
while True:
    guess = input('Your guess: ')
    count_cow = 0
    count_bull = 0
    if guess == number:
        print('You won!')
        break
    else:
        for i in range(len(guess)):
            if guess[i] in number:
                if guess.index(guess[i]) == number.index(guess[i]):
                    count_bull += 1
                else:
                    count_cow += 1
        print(f'{count_cow} cow(s), {count_bull} bull(s).')
        continue


# 2. Like
# Создайте программу, которая, принимая массив имён,
# возвращает строку описывающую количество лайков (как в Facebook).
# Введите имена через запятую: "Ann"
# -> "Ann likes this"
names = input('Введите имена через запятую: ').split(', ')

if len(names) == 1 and names[0] != '':
    print(f'{names[0]} likes this')
elif len(names) == 2:
    print(f'{names[0]} and {names[1]} like this')
elif len(names) == 3:
    print(f'{names[0]}, {names[1]} and {names[2]} like this')
elif len(names) > 3:
    print(f'{names[0]}, {names[1]} and {len(names) - 2} others like this')
else:
    print('No one likes this')

# 3. BuzzFuzz
# Напишите программу, которая перебирает последовательность от 1 до 100.
# Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа,
# а для чисел кратных 5  печатать "Buzz".
# Для чисел которые кратны 3 и 5 надо печатать "FuzzBuzz".
# Иначе печатать число.
for number in range(1, 101):
    if number % 3 == 0 and number % 5 != 0:
        print('Fuzz')
    elif number % 5 == 0 and number % 3 != 0:
        print('Buzz')
    elif number % 3 == 0 and number % 5 == 0:
        print('FuzzBuzz')
    else:
        print(number)


"""4. Напишите код, который возьмет список строк и пронумерует их.
Нумерация начинается с 1, имеет “:” и пробел
[] => []
["a", "b", "c"] => ["1: a", "2: b", "3: c"]
"""
orig_list = ["a", "b", "c"]
res_list = list()

for item in orig_list:
    item = "{}: ".format(len(res_list) + 1) + item
    res_list.append(item)
    print(item)

print("{} => {}".format(orig_list, res_list))

"""Проверить, все ли элементы одинаковые
"""
a = [1, 1, 1]  # == True
b = [1, 2, 1]  # == False
c = ['a', 'a', 'a']  # == True
g = []          # == True


def check_same_elements(param):
    is_the_same = "True"
    for elm in param:
        for elm_1 in param:
            if elm == elm_1:
                print("True")
            else:
                print("False")
                is_the_same = "False"
                break
        if is_the_same == "False":
            break
    return is_the_same


print(check_same_elements(a))
print(check_same_elements(b))
print(check_same_elements(c))
print(check_same_elements(g))


def check_same_elements_easier(param):
    is_the_same = "True"
    prev_elem = None

    for elm in param:
        if prev_elem is not None:
            if elm != prev_elem:
                is_the_same = "False"
                break
        print(prev_elem, elm)

        prev_elem = elm
    return is_the_same


print(a, check_same_elements_easier(a))
print(b, check_same_elements_easier(b))
print(c, check_same_elements_easier(c))
print(g, check_same_elements_easier(g))

""" 6.
Проверка строки. В данной подстроке проверить все ли буквы в
строчном регистре или нет и вернуть список не подходящих.
"""
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]


string = "doGCat"

resFullList = list()

resFullList.append("True")

resFullList.append(list())

resOffset = 0

bigLettersOffset = 1


for letter in string:
    if not letter.islower():
        resFullList[resOffset] = "False"
        resFullList[bigLettersOffset].append(letter)

print("{} => {}".format(string, resFullList))

"""7. Сложите все числа в списке, они могут быть
отрицательными,если список пустой вернуть 0
[] == 0
[1, 2, 3] == 6
[1.1, 2.2, 3.3] == 6.6
[4, 5, 6] == 15
range(101) == 5050not

"""
all_sum = 0
sum_list = [1.1, -2.2, 3.3]
rang = range(101)
d = sum(rang)
print(sum_list, d)

# for i in sum_list:
#    all_sum += i
# print(sum_list, all_sum)
