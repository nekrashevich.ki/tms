import math
"""Даны 2 действительных числа a и b. Получить их сумму, разность и
произведение"""

a = 1  # valid numbers
b = 2  # valid numbers

c1 = a + b  # Sum of valid numbers
c2 = a - b  # Residual of valid numbers
c3 = a * b  # multiplication of valid numbers

print('Sum', c1)
print('Residual', c2)
print('malt', c3)

"""Даны действительные числа x и y. Получить  |x| − |y| / 1+ |xy| """

x = 3  # valid numbers
y = 2  # valid numbers

mod_x = x if x > 0 else -x
mod_y = y if y > 0 else -y
c = mod_x - (mod_y / 1) + (mod_x * mod_y)  # Formula

print('Result', c)

"""Дана длина ребра куба. Найти объем куба и площадь его боковой
поверхности."""

a = 2  # valid numbers

s = 6 * (a * a)     # formula for finding S of the lateral surface
v = a * a * a       # formula for finding the volume

print('S of the lateral surface', s)
print('volume', v)

"""Даны два действительных числа. Найти среднее арифметическое и
среднее геометрическое этих чисел"""

x = 3  # valid numbers
y = 5  # valid numbers

g1 = (x + y) / 2     # formula arithmetical mean
g2 = math.sqrt(x * y)  # formula geometric mean

print('geometric mean', g2)
print('arithmetical mean', g1)

"""Даны катеты прямоугольного треугольника. Найти его гипотенузу и
площадь."""

a = 3  # first leg
b = 4  # second leg

c = math.sqrt(a ^ 2 + b ^ 2)  # formula of the hypotenuse
s = 0.5 * a * b               # formula area of a triangle

print('hypotenuse', c)
print('area of triangle', s)
