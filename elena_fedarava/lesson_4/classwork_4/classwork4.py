# Lists
# Task1.1: Создайте два любых списка и свяжите их с переменными
arr1 = [1, 2, 3, 4, 5]
arr2 = [6, 7, 8, 9, 10]

# Task1.2: Извлеките из первого списка второй элемент.
print('Task 1.2:', arr1[1])

# Task1.3: Измените во втором списке последний элемент. Выведите список на
# экран
arr2[len(arr2) - 1] = 9
print('Task 1.3:', arr2)

# Task1.4: Соедините оба списка в один, присвоив результат новой переменной.
# Выведите получившийся список на экран
arr12 = arr1 + arr2
print('Task 1.4:', arr12)

# Task1.5: "Снимите" срез из соединенного списка так, чтобы туда попали
# некоторые части обоих первых списков. Срез свяжите с очередной новой
# переменной. Выведите значение этой переменной.
middle_arr12 = int((len(arr1) + len(arr2)) / 2)
arr12_cut = arr12[middle_arr12 - 2:middle_arr12 + 2]
print('Task 1.5:', arr12_cut)

# Task1.6: Добавьте в список два новых элемента и снова выведите его.
new_elements = ['New', 'list']
arr12_cut.extend(new_elements)
print('Task 1.6:', arr12_cut)

# Task1.7: Даны списки:
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
# b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13] Нужно вернуть список, который
# состоит из элементов, общих для этих двух списков. -- !не использовать циклы
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
nf = [x for x in b if x in a]
print('Task 1.7:', nf)

# Task1.8: Есть список: [1, 2, 3, 4, 3, 2, 5, 1, 4, 6, 7, 1, 8, 2, 3] оставить
# в нем только уникальные значения. !не использовать циклы
arr3 = [1, 2, 3, 4, 3, 2, 5, 1, 4, 6, 7, 1, 8, 2, 3]
unique_arr3 = list(set(arr3))
print('Task 1.8:', unique_arr3)

# Logical operations
# Task2.1: Присвойте двум переменным любые числовые значения.
ran1 = 1
ran2 = 2

# Task2.2: Используя переменные из п. 1, с помощью оператора and составьте два
# сложных логических выражения, одно из которых дает истину, другое – ложь.
print('Task 2.2 TRUE:', ran1 != 0 and ran2 != 1)
print('Task 2.2 FALSE:', ran1 != 0 and ran2 != 2)

# Task2.3: Аналогично выполните п. 2, но уже с оператором or.
print('Task 2.3 TRUE:', ran1 != 0 or ran2 != 1)
print('Task 2.3 FALSE:', ran1 == 0 or ran2 == 1)

# Task2.4: Попробуйте использовать в логических выражениях переменные
# строкового типа. Объясните результат.
log_str1 = 'qwerty'
log_str2 = '123'
print('Task 2.4 TRUE:', len(log_str1) > len(log_str2))  # 1ая строка длинее 2ой
print('Task 2.4 FALSE:', log_str1[0] == log_str2[0])  # 1ый элемент 2ух
# списков не одинаковый

# Task2.5: Напишите программу, которая запрашивала бы у пользователя два числа
# и выводила бы True или False в зависимости от того, больше первое число
# второго или нет. (используйте функцию input())
first_number = int(input('Task 2.5: Введите первое число: '))
second_number = int(input('Task 2.5: Введите второе число: '))
print('Task 2.5:', max(first_number, second_number) == first_number and
      max(first_number, second_number) != second_number)

# Dictionaries
# Task3.1: Создайте словарь, связав его с переменной school, и наполните его
# данными, которые бы отражали количество учащихся в десяти разных классах
# (например, 1а, 1б, 2б, 6а, 7в и т.д.).
school = {'1a': 19, '1b': 20, '2b': 30, '6a': 21, '7c': 24}

# Task3.2: Узнайте сколько человек в каком-нибудь классе
print('Task 3.2:', school['1a'])

# Task3.3: Представьте, что в школе произошли изменения, внесите их в словарь:
# в трех классах изменилось количество учащихся;
# в школе появилось два новых класса;
# в школе расформировали один из классов
school['1b'] = 22  # изменилось кол-во учеников
school['2b'] = 28  # изменилось кол-во учеников
school['7c'] = 25  # изменилось кол-во учеников
school['7a'] = 10  # новый класс
school['7b'] = 10  # новый класс
del school['6a']  # класс расформирован

# Task3.4: Выведите содержимое словаря на экран.
print('Task 3.4:', school)

# Tuples
# Task4.1: Создайте кортеж с цифрами от 0 до 9 и посчитайте сумму.
# (ожидаемый вывод суммы = 45)
tp = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
print('Task 4.1:', sum(tp))

# Task4.2: Есть кортеж long_word = ('т', 'т', 'а', 'и', 'и', 'а', 'и', 'и',
# 'и', 'т', 'т', 'а', 'и', 'и', 'и', 'и', 'и', 'т', 'и')
# распечатайте на экран количество букв “т”, “и”, “а”
long_word = ('т', 'т', 'а', 'и', 'и', 'а', 'и', 'и', 'и', 'т', 'т', 'а', 'и',
             'и', 'и', 'и', 'и', 'т', 'и')
print('Task 4.2:', 'т:', long_word.count('т'), 'и:', long_word.count('и'),
      'а:', long_word.count('а'))

# Task4.3: Допишите скрипт для расчета средней температуры.
# Постарайтесь посчитать количество дней на основе week_temp.
# Так наш скрипт сможет работать с данными за любой период.
# данный код
# week_temp = (26, 29, 34, 32, 28, 26, 23)
# sum_temp =
# days =
# mean_temp = sum_temp / days
# print(int(mean_temp))
# требуемый вывод:
# 28
week_temp = (26, 29, 34, 32, 28, 26, 23)
sum_temp = sum(week_temp)
days = len(week_temp)
mean_temp = sum_temp / days
print('Task 4.3:', int(mean_temp))
